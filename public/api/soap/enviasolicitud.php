<?php

$endpoint = "https://10.209.3.130/ABDCPWebApp/services/ABDCPWebService?wsdl";

$client = new SoapClient($endpoint, array(
    "uri"      => "",
    "trace"    => 1,
));

$xmlPrueba = '<?xml version="1.0" encoding="UTF-8"?>
<MensajeABDCP>
	<CabeceraMensaje>
		<IdentificadorMensaje>61201407170050001</IdentificadorMensaje>
		<Remitente>61</Remitente>
		<Destinatario>00</Destinatario>
		<FechaCreacionMensaje>'.date('YmdHmi').'</FechaCreacionMensaje>
		<IdentificadorProceso>61201407170050001</IdentificadorProceso>
	</CabeceraMensaje>
	<CuerpoMensaje IdMensaje="SP">
		<SolicitudPortabilidad>
			<CodigoReceptor>61</CodigoReceptor>
			<CodigoCedente>20</CodigoCedente>
			<TipoDocumentoIdentidad>05</TipoDocumentoIdentidad>
			<NumeroDocumentoIdentidad>1</NumeroDocumentoIdentidad>
			<CantidadNumeraciones>1</CantidadNumeraciones>
			<NumeracionSolicitada>
				<RangoNumeracion>
					<InicioRango>73269953</InicioRango>
					<FinalRango>73269953</FinalRango>
					<TipoPortabilidad>01</TipoPortabilidad>
				</RangoNumeracion>
			</NumeracionSolicitada>
			<Observaciones/>
			<NombreContacto>Test</NombreContacto>
			<EmailContacto>a@a.com</EmailContacto>
			<TelefonoContacto>73269901</TelefonoContacto>
			<FaxContacto>73269901</FaxContacto>
			<TipoServicio>1</TipoServicio>
			<Cliente>2</Cliente>
		</SolicitudPortabilidad>
	</CuerpoMensaje>
</MensajeABDCP>';

$xmlPrueba = htmlentities($xmlPrueba);

$test = "
  <soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:ws=\"http://ws.inpac.telcordia.com\">
   <soap:Header/>
   <soap:Body>
      <ws:receiveMessageRequest>
         <ws:userID>61u1</ws:userID>
         <ws:password>UDYxdTEy</ws:password>
         <ws:xmlMsg>$xmlPrueba</ws:xmlMsg>
      </ws:receiveMessageRequest>
   </soap:Body>
</soap:Envelope>";
 
try{
    $order_return = $client->__doRequest($test,$endpoint,$endpoint,2);
    $pos1 = strpos($order_return, "<ns1:response>");
    $pos2 = strpos($order_return, "</ns1:response>");
    $r2 = substr(substr($order_return, $pos1,  $pos2-$pos1),  strlen("<ns1:response>"));
    var_dump($r2);
    
} catch (SoapFault $exception){
    var_dump(get_class($exception));
    var_dump($exception);
}
